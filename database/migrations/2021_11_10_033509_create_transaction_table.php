<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('transaction_id')->primary();
            $table->string('customer_name');
            $table->string('customer_code');
            $table->integer('transaction_amount');
            $table->float('transaction_discount')->nullable();
            $table->float('transaction_additional_field')->nullable();
            $table->integer('transaction_payment_type');
            $table->string('transaction_state');
            $table->string('transaction_code');
            $table->string('transaction_order');
            $table->uuid('location_id');
            $table->integer('organization_id');
            $table->integer('transaction_payment_type_name');
            $table->integer('transaction_cash_amount');
            $table->integer('transaction_cash_change');
            $table->integer('customer_attribute_id');
            $table->uuid('connote_id');
            $table->integer('origin_data_id');
            $table->integer('destination_data_id');
            $table->json('custom_field')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
