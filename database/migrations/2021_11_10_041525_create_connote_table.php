<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConnoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connote', function (Blueprint $table) {
            $table->uuid('connote_id')->primary();
            $table->integer('connote_number');
            $table->string('connote_service');
            $table->integer('connote_service_price');
            $table->integer('connote_amount');
            $table->string('connote_code');
            $table->string('connote_booking_code')->nullable();
            $table->integer('connote_order');
            $table->string('connote_state');
            $table->integer('connote_state_id');
            $table->string('zone_code_from');
            $table->string('zone_code_to');
            $table->string('surcharge_amount');
            $table->uuid('transaction_id');
            $table->float('actual_weight');
            $table->float('volume_weight');
            $table->float('chargeable_weight');
            $table->integer('organization_id');
            $table->uuid('location_id');
            $table->string('connote_total_package');
            $table->string('connote_surcharge_amount');
            $table->string('location_name');
            $table->string('location_type');
            $table->string('source_tariff_db');
            $table->string('id_source_tariff');
            $table->string('pod')->nullable();
            $table->json('history');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connote');
    }
}
