<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKoliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koli', function (Blueprint $table) {
            $table->uuid('koli_id')->primary();
            $table->integer('koli_length');
            $table->string('awb_url');
            $table->integer('koli_chargeable_weight');
            $table->integer('koli_width');
            $table->text('koli_surcharge');
            $table->integer('koli_height');
            $table->string('koli_description');
            $table->integer('koli_formula_id')->nullable();
            $table->uuid('connote_id');
            $table->integer('koli_volume');
            $table->integer('koli_weight');
            $table->json('koli_custom_field');
            $table->string('koli_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koli');
    }
}
